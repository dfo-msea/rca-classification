# Classification of Rockfish Conservation Areas (RCAs)

__Main author:__  Jessica Nephin   
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@dfo-mpo.gc.ca | tel: 250.363.6564


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Cluster RCAs into groups based on habitat, geographic and human use variables and select a few representative RCAs from each group.

## Summary
This classification analysis is part of the development of a long term RCA monitoring plan. The purpose of this work is to inform that monitoring by identifying representative RCAs from each cluster to monitor.

## Status
In-development

## Contents
This project contains a number of python jupyter notebooks. There is a notebook for the classification analysis (k-means clustering) and the selection of representative RCAs. There are additional notebooks for calculating the habitat and human use variables from the input spatial data layers. The geographic variables had already been calculated in a previous analysis.

## Methods
### Variable calculations
To calculate variables from the numerous spatial data layers, rasters values or point, line and polygon features were summarised for each RCA. The summary statistics (e.g., mean, sum, count) that were employed varied by layer depending on the variable of interest. See the individual notebooks for details.

### Classification methods
* Load previously calculated variables, transform and standardize as needed.
* Look for correlations between variables using plots and variance inflation factor and remove variables that are highly correlated with other variables.
* Calculate the principle components (PCA) from the set of variables and examine the amount of variation they explain.
* Select the optimal number of clusters (k) qualitatively by plotting mean inertia (elbow plot) and silhouette coefficient by number of clusters.
* Create k clusters from the principle components using the kmeans clustering algorithm (https://scikit-learn.org/stable/modules/clustering.html).
* Calculate the squared distance in principle component space between each RCA and its cluster centroid as a measure of fit.

### Selection methods
* Select the number of RCAs that you want to represent each cluster (e.g. 2 or 3) within each subregion
* The RCAs are selected by prioritizing previously sampled RCAs (in/out RCA transects) and then by selecting the RCAs with the lowest squared distances for each cluster.
* The number of representative RCAs are then reduced by randomly sampling using the occurrence of previous sampling and the relative rarity of the cluster for weighting. Thus preferentially selecting RCAs with previous samples and those from less well represented clusters. This helps to ensure that are clusters are represented evenly.
* Outlier RCAs are also identified by locating RCAs that have very high values in a given variable in only 3 or less RCAs and very low values in all other RCAs (highly skewed distribution).
* The RCA with the maximum value for tidal current speed is also considered an outlier because this important variable wasn't included in the clustering analysis due to data quality concerns in the QCS region.

## Requirements
* Python 3.7
* Jupyter notebook
* A number of addition python packages (see imports in notebooks)

## Caveats
Classification can be subjective due to the qualitative methods used to select the number of clusters. The results may vary depending on how the user interprets the elbow and silhouette plots. In addition, the choice of clustering algorithm can effect which sites fall into which clusters, k-means clusters may not be appropriate for all applications.

## Uncertainty
Uncertainty in the  selection of clusters and representative RCAs comes from the quality of the input data, which is often unknown or at least not directly evaluated. However, an attempt at measuring the uncertainty of the bathymetry data has been made (example: https://gitlab.com/dfo-msea/environmental-layers/bathy-uncertainty) which has been used here to identify where habitat data gaps exist.  

## Acknowledgements
Cole Fields (https://gitlab.com/dfo-msea/environmental-layers/bathy-derivatives)
